<?php

App::uses('ReportManagerAppController', 'ReportManager.Controller');
App::uses('Croogo', 'Lib');

class ElectricCooperativesController extends ReportManagerAppController {
    public $uses = array('ReportManager.ElectricCooperative');

    public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow(array('ec_list'));
	}

    public function ec_list(){

    	$electricCooperatives = $this->ElectricCooperative->find('list', array('order' => 'ElectricCooperative.abbreviation ASC'));
    	$this->set(compact('electricCooperatives'));

    }


}