<?php

App::uses('Component', 'Controller');

class IMCGlobalComponent extends Component {



	public function generateCode($s_length = null, $n_length = null) {

			$str='';
			$num='';
			$total_length = $s_length + $n_length ;
			$continue = true;
			do {

				if ($s_length) {
					$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";	
					$str = '';
					$size = strlen( $chars );
					for( $i = 0; $i < $s_length; $i++ ) {
						$str .= $chars[ rand( 0, $size - 1 ) ];
					}
					$str = strtoupper($str);
				} else {
					$str = '';
				}	
	
				if ($n_length) {
					$nums = "123456789";
					$num = '';
					$size = strlen($nums);
					for( $i = 0; $i < $n_length; $i++ ) {
						$num .= $nums[ rand( 0, $size - 1 ) ];
					}
					$num = strtoupper($num);
				} else {
					$num = '';
				}
		
					$gen_string = strlen($str) + strlen($num);
					
					if ($total_length == $gen_string) {
						$continue = false;
					}
			
			} while  ($continue);
			
			return $str.$num;

		}


		public function regInformer($firstName = null, $lastName = null, $address = null, $email = null, $mobile = null, $type = null){

			
			
			$this->Informer = ClassRegistry::init('ReportManager.Informer');

				$wb_pass = $this->generateCode(4, 4);
				
				if ($this->Informer->findByMobile($mobile)) {
					return false;
			
				}else if ($this->Informer->findByEmail($email)) {
			
					return false;	

				} else {
					
					$continue = true;
					do {

						$Informer['Informer']['code']  = trim($this->generateCode(null, 10));

						if(!$this->Informer->findByCode($Informer['Informer']['code'])){

							if($firstName){
								$Informer['Informer']['first_name'] = trim($firstName);
							}
							if($lastName){
								$Informer['Informer']['last_name'] = trim($lastName);
							}
							if($address){
								$Informer['Informer']['address'] = trim($address);
							}

							if($mobile){
								$Informer['Informer']['mobile'] = trim($mobile);
							}
							if($email){
								$Informer['Informer']['email'] = trim($email);
							}
							
							if($type){
								$Informer['Informer']['type'] = trim($type);
							}

							if($wb_pass){
								$Informer['Informer']['password'] = trim($wb_pass);
							}

							if ($this->Informer->save($Informer)) {
								$wb_id = $this->Informer->getLastInsertID();
								$continue = false;
							}
						}

					} while ($continue) ;
	
					$wb_code = $Informer['Informer']['code'];
					
					$wb = array('id' => $wb_id, 'code' => $wb_code);
					return $wb;

				}
				
			

		}


}

?>	