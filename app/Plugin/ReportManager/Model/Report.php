

<?php
/**
 * Report Model
 *
 */
class Report extends ReportManagerAppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useDbConfig = 'system';
	public $useTable = 'complains';
	public $displayField = 'report_code';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Informer' => array(
			'className' => 'ReportManager.Informer',
			'foreignKey' => 'wblower_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ReportStatus' => array(
			'className' => 'ReportManager.ReportStatus',
			'foreignKey' => 'complain_condition_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ElectricCooperative' => array(
			'className' => 'ReportManager.ElectricCooperative',
			'foreignKey' => 'utility_name_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Finding' => array(
			'className' => 'ReportManager.Finding',
			'foreignKey' => 'finding_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

	

}
