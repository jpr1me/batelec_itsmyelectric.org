<?php


class Informer extends AppModel {

	public $useDbConfig = 'system';
	public $useTable = 'wblowers';
	public $displayField = 'username';


	public $hasMany = array(
		'Report' => array(
			'className' => 'ReportManager.Report',
			'foreignKey' => 'wblower_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}


?>