<?php
//App::uses('AppModel', 'Model');
/**
 * ReportStatus Model
 *
 * @property Report $Report
 */
class ReportStatus extends ReportManagerAppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useDbConfig = 'system';
	public $useTable = 'complain_conditions';
	public $displayField = 'code';


	//The Associations below have been created with all possible keys, those that are not needed can be removed


/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Report' => array(
			'className' => 'ReportManager.Report',
			'foreignKey' => 'complain_condition_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);

}
