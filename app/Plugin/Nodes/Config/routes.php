<?php

// Basic
CroogoRouter::connect('/', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'promoted'
));

CroogoRouter::connect('/promoted/*', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'promoted'
));

CroogoRouter::connect('/search/*', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'search'
));

// Content types
CroogoRouter::contentType('blog');
CroogoRouter::contentType('node');
if (Configure::read('Croogo.installed')) {
	CroogoRouter::routableContentTypes();
}

// Page
CroogoRouter::connect('/about', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'view',
	'type' => 'page', 'slug' => 'about'
));
CroogoRouter::connect('/page/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'view',
	'type' => 'page'
));

// Plugin

CroogoRouter::connect('/reports', array(
	'plugin' => 'report_manager', 'controller' => 'reports', 'action' => 'index',
	'type' => 'page', 'slug' => 'reports'
));

CroogoRouter::connect('/stats', array(
	'plugin' => 'report_manager', 'controller' => 'reports', 'action' => 'stats',
	'type' => 'page', 'slug' => 'reports'
));

CroogoRouter::connect('/ec_list', array(
	'plugin' => 'report_manager', 'controller' => 'electric_cooperatives', 'action' => 'ec_list',
	'type' => 'page', 'slug' => 'ec_list'
));

CroogoRouter::connect('/ec_stats', array(
	'plugin' => 'report_manager', 'controller' => 'reports', 'action' => 'ec_stats',
	'type' => 'page', 'slug' => 'ec_stats'
));
